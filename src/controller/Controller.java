package controller;

import java.util.ArrayList;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.Manejador;

public class Controller 
{
	private static Manejador manejador = new Manejador();
	
	public static Stack crearExpresionStack(ArrayList<Character> values)
	{
		return new Stack(values);
	}
	
	public static String crearExpresion(ArrayList<Character> values)
	{
		String retornar = "";
		for(int i=0; i<values.size();i++)
		{
			retornar = retornar + values.get(i).charValue();
		}
		return retornar;
	}
	
	public static boolean expresionBienFormada(String expresion)
	{
		return manejador.expresionBienFormada(expresion);
	}
	
	public static String ordenarPila()
	{
		return manejador.ordenarPila();
	}
}
