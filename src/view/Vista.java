package view;

import java.util.ArrayList;
import java.util.Scanner;
import controller.Controller;
import model.data_structures.Stack;

public class Vista 
{
	public static void main(String[] args)
	{
		Stack lista =  null;
		String lista2 = null;
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
			case 1:
				lista2 =  Controller.crearExpresion(readData());
				//lista = Controller.crearExpresionStack(readData());
				System.out.println("--------- \n La expresión fue creada  \n---------");
				break;
			case 2:
				String expresion = lista2.toString();
				if(Controller.expresionBienFormada(expresion))
				{
					System.out.println("La expresión esta bien formada");
				}
				else
					System.out.println("La expresión no esta bien formada");
				break;
			case 3:
				System.out.println(Controller.ordenarPila());
				break;
			case 4: System.out.println("ˇˇAdios!!  \n---------"); 
				sc.close(); 
				return;	
			}
		}
	}
	
	private static ArrayList<Character> readData()
	{
		ArrayList<Character> data = new ArrayList<Character>();
		
		System.out.println("Por favor ingresa la expresión y luego presiona enter: ");
		try
		{
			String line = new Scanner(System.in).nextLine();
		
			char[] values = line.toCharArray();
			for(int i=0; i<values.length;i++)
			{
				data.add(values[i]);
			}
		}
		catch(Exception ex)
		{
			System.out.println("Please type an expression");
		}
		return data;
	}
	
	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Ingrese una expresión");
		System.out.println("2. Comprobar que la expresión esta bien formada");
		System.out.println("3. Ordenar expresión");
		System.out.println("4. Salir");
		System.out.println("Elige un numero para realizar la tarea:");
		
	}
}
